//npm модули
// Существует 3 модели
// await faceapi.detectAllFaces(input, new faceapi.SsdMobilenetv1Options())
// await faceapi.detectAllFaces(input, new faceapi.TinyFaceDetectorOptions())
// await faceapi.detectAllFaces(input, new faceapi.MtcnnOptions())


const path = require('path')
var multiparty = require('multiparty');
var format = require('util').format;
const fs = require('fs');
var express = require('express');
var bodyParser = require('body-parser');
var mongoClient = require('mongodb').MongoClient;
var crypto = require('crypto');
var faceapi = require("face-api.js");

var commons = require('./commons');
	
var urlDataBase = "mongodb://127.0.0.1:27017/models";
var collectionDataBase = 'md';
var collectionConfig ='config';
var jsonParser = bodyParser.json();
var port = 3001;
var numTrainFaces = 5;
var password;
var db1;
var salt = '12345'; //соль для пароля

var app = express();
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb'}));
app.use(express.static(path.join(__dirname, '../face/commons/weights'))) // модели
app.use(express.static(path.join(__dirname, '../face/commons/dist'))) // библиотека

//считывание настроек из файла
if(fs.existsSync('./config.txt')){
	var cfg = fs.readFileSync('./config.txt');
	var cfg_js = JSON.parse(cfg);
	urlDataBase = cfg_js.urlDataBase;
	collectionDataBase = cfg_js.collectionDataBase;
	collectionConfig = cfg_js.collectionConfig;
	port = cfg_js.port;
}

commons.ensureAppdataDirExists();

function SetDefaultConfig(err,result){
    if(err){
        var msg = {
            status: 'err',
            info: err
        }
        console.log(msg);
    }
    else{
        var msg = {
            status: 'ok',
            info: 'Create user admin admin'
        }
        console.log(msg);
	db1.close();
    }
};

mongoClient.connect(urlDataBase,function ConnectToDB(err, result){
    if(err){
        throw err;
    }else{
        db1 = result;
        db1.collection(collectionConfig)
	  .find()
	  .toArray(function FindAdmin(err, result) {
    		if (err) {
        		var msg = {
       				status: 'err',
            			info: err
        		}
        		console.log(msg);
    		}
    		if (result.length == 0) {
			var md5Hash = crypto.createHash('md5')
				.update('admin')
				.update(salt)
				.digest('base64');
        		var admin = {
            			login: 'admin',
           			password: md5Hash
        		}
			password = md5Hash;
			console.log('insert');
        		db1.collection(collectionConfig).insertOne(admin, SetDefaultConfig)
    		}
    		else{
        		var msg = {
            			status: 'ok',
            			info: 'User admin exist'
        		}
			password = result[0].password;
        		console.log(msg);
    		}
	});
    }
});

app.get("/", function(request,response){
	response.end("service FaceRec work`s");
});

app.post("/changePass", jsonParser, async function (request, response) {
    //параметры request.body
    //	password - старый пароль
    //	newpassword - новый пароль
    if(!request.body) return response.sendStatus(400);
    var md5Hash = crypto.createHash('md5')
			.update(request.body.password)
			.update(salt)
			.digest('base64');
    var md5HashNew = crypto.createHash('md5')
			.update(request.body.newpassword)
			.update(salt)
			.digest('base64');
    mongoClient.connect(urlDataBase,function (err, db) {
		if(err){
			throw err;
		}
		else{
			db.collection('config').find({login:'admin'},{password:1}).toArray(
				function (err, admin){
					if(err){
						throw err;
					}
					else{
						if(admin[0].password == md5Hash){
                            db.collection(collectionConfig)
			      				.findOneAndUpdate(
                                {login: 'admin'},
                                {$set: {password: md5HashNew}},
                                {returnOriginal: false, upsert: true},
                                function(err, result){
                                    var msg = {
                                        status: 'ok',
                                        info: 'password changed'
                                    }
				    password = md5HashNew;
                                    response.end(JSON.stringify(msg));
                                }
                            );
						}
						else
						{
							var msg = {
								status: 'err',
								info: 'wrong password'
							}
							response.end(JSON.stringify(msg));
						}
					}
				}
			);
		}
    });
});

app.post("/train", jsonParser, async function (request, response) {
	//параметры request.body
	//	password - пароль
	//  countFaces - число лиц для тренировки
	//	studId - имя/id-студента
	//  type - расшиение изображения без точки
	//	images - массив изображений
	//	все изображения передаются в виде строки base64
	numTrainFaces = request.body.countFaces;
	if(!request.body) return response.sendStatus(400);
	var md5Hash = crypto.createHash('md5')
			.update(request.body.password)
			.update(salt)
			.digest('base64');
	if(md5Hash != password){
		console.log('wrong password');
		var rezult = {
			status: "err",
			info: "wrong password"
		};
		response.end(JSON.stringify(rezult));
		return;
	}
	console.log("upload begins...");
	let images = [];
	for(i=0;i<numTrainFaces;i++){
		images[i] = commons.readImFromBase64(request.body.images[i], request.body.studId,'.'+request.body.type,i+1);
	}
	console.log("upload end");

	console.log("load models..."); 
	let faces = [];
	 let fullFaceDescriptions = [];
	 console.log(faceapi.nets);
	try{
		await faceapi.nets.ssdMobilenetv1.loadFromDisk('../face/commons/weights');
		await faceapi.nets.tinyFaceDetector.loadFromDisk('../face/commons/weights');
		await faceapi.nets.mtcnn.loadFromDisk('../face/commons/weights');
		await faceapi.nets.faceRecognitionNet.loadFromDisk('../face/commons/weights');
		await faceapi.nets.faceLandmark68Net.loadFromDisk('../face/commons/weights');
	}
	catch(e){
	 	console.log('Error load models!');
	}
	for(i=0;i<numTrainFaces;i++)
	{		
		console.log("Load image");
		image = await commons.canvas.loadImage(images[i]);
		console.log('detecting faces for image...');
		//,  new faceapi.TinyFaceDetectorOptions()
		fullFaceDescriptions = await faceapi.detectAllFaces(image, new faceapi.MtcnnOptions()).withFaceLandmarks().withFaceDescriptors();

		//проверка есть ли на фотографии лицо вообще
		if(fullFaceDescriptions.length != 1)
		{
			var msg = {
				status: "err",
				info: "not find faces on image or faces more than one"
			}
			response.end(JSON.stringify(msg));
			console.log("not find faces on image or faces more than one");
			//удаление файлов
			for(i=0;i<numTrainFaces;i++){
				fs.unlinkSync(images[i]);
			}
			console.log("image deleted");
			return;
		}
		else{
			faces.push(fullFaceDescriptions[0].descriptor);
		}
	}

	console.log("read ok");	
	
	console.log("start trainer recognizer");

	//запись в бд (если запись есть обновляем, если нет просто добавляем)
	await mongoClient.connect(urlDataBase, function(err, db){
		if(err){
			throw err;
		}else{
			console.log("connect to database done" + urlDataBase);
			db.collection(collectionDataBase).findOneAndUpdate(
				{studId: request.body.studId},
				{$set: {frModel: JSON.stringify(faces),
				numTrainFaces: numTrainFaces,
				date : Date()}},
				{returnOriginal: false, upsert: true},
				function(err, result){
					console.log("ok:"+result.ok+" "+JSON.stringify(result.lastErrorObject));
				}
			);
			db.close();
		}
	});


	console.log("recognizer create");
	//ответ серверу
	var msg = {
		status: "ok",
		info: `recognizer frModel_${request.body.studId}_t${numTrainFaces}.json create`
	};
	response.end(JSON.stringify(msg));
	//удаление файлов
	for(i=0;i<numTrainFaces;i++){
		fs.unlinkSync(images[i]);
	}
	console.log("image deleted");
});

app.post("/recognition", jsonParser, async function(request, response){
	//параметры request.body
	//	password - пароль
	//	studId - имя/id-студента
	//	type - расширение изображения (без точки)
	//	image - изображение (строка base64)

	// создаём рамку в которой должно находиться лицо
	const displaySize = {height: 200, width: 250, x: 50, y: 20};
	var box = new faceapi.Box(displaySize);
	var faceinrect = "";
	var expressions = "";
	var age;
	var gender = "";
	response.setHeader('Content-Type','application/json');
	if(!request.body) return response.sendStatus(400);
	var md5Hash = crypto.createHash('md5')
			.update(request.body.password)
			.update(salt)
			.digest('base64');
	if(md5Hash != password){
		console.log('wrong password');
		var rezult = {
			status: "err",
			info: "wrong password"
		};
		response.end(JSON.stringify(rezult));
		return;
	}
	try{
		//await faceapi.nets.ssdMobilenetv1.loadFromDisk('../face/commons/weights');
		//await faceapi.nets.tinyFaceDetector.loadFromDisk('../face/commons/weights');
		await faceapi.nets.mtcnn.loadFromDisk('../face/commons/weights');
		await faceapi.nets.faceRecognitionNet.loadFromDisk('../face/commons/weights');
		await faceapi.nets.faceExpressionNet.loadFromDisk('../face/commons/weights');
		await faceapi.nets.ageGenderNet.loadFromDisk('../face/commons/weights');
		await faceapi.nets.faceLandmark68Net.loadFromDisk('../face/commons/weights');	
	}
	catch(e){
		console.log('Ошибка в загрузке моделей!');
	}
	console.log("connect to database...");
	mongoClient.connect(urlDataBase, async function(err, db){
		if(err){
			throw err;
			console.log("err");
		}
		else{
			console.log("connect to database done");;
			db.collection(collectionDataBase)
                          .find({studId: request.body.studId},{frModel: 1}) 
                          .toArray(async function(err, stud){
				if(stud.length == 0){
				   var msg = {
					status : "unknow",
					studId : "unknow",
					distance : 1,
					info: "don`t find student "+request.body.studId +" on database"
				   };
				   console.log("don`t find student "+request.body.studId +" on database");
				   response.end(JSON.stringify(msg));
				}
				else{
					console.log("find stud "+request.body.studId);
					
					let model = JSON.parse(stud[0].frModel);			
					let descriptors = [];

					for(i=0;i<model.length;i++){
						descriptors[i] = await new Float32Array(Object.values(model[i]));
					}
					
					const modelFaceDescriptors = await new faceapi.LabeledFaceDescriptors(request.body.studId, descriptors);
					const faceMatcher = new faceapi.FaceMatcher(modelFaceDescriptors, 0.6)

                    var ImageFilePath = './images/'+request.body.studId +'.'+ request.body.type;
                    var buff = Buffer.from(request.body.image.replace(/^data:image\/(png|gif|jpeg);base64,/,''),'base64');
                    fs.writeFileSync(ImageFilePath, buff);
					console.log(`file ${ImageFilePath} upload`);

					console.log("recognition begin...");
					
					var image = await commons.canvas.loadImage(ImageFilePath);

					console.log('detecting faces for image...');

					//,  new faceapi.TinyFaceDetectorOptions()
					var fullFaceDescriptions = await faceapi.detectAllFaces(image, new faceapi.MtcnnOptions())
															.withFaceLandmarks()
															.withFaceExpressions()
															.withAgeAndGender()
															.withFaceDescriptors();
					
					// нахождение лица в заданной рамке
					if (box.x <= fullFaceDescriptions[0].detection._box.x &&
						box.y <= fullFaceDescriptions[0].detection._box.y &&
						(fullFaceDescriptions[0].detection._box.x + fullFaceDescriptions[0].detection._box.width) <= (box.x + box.width) &&
						(fullFaceDescriptions[0].detection._box.y + fullFaceDescriptions[0].detection._box.height) <= (box.y + box._height))
								faceinrect = "yes";
					else
						faceinrect = "no";					
					
					age = fullFaceDescriptions[0].age;
					gender = fullFaceDescriptions[0].gender;
					expressions = fullFaceDescriptions[0].expressions;
					
					// разобрать эмоции
					var arr = [];
					arr.push({
						key: "angry",
						value: fullFaceDescriptions[0].expressions.angry
					});
					arr.push({
						key: "disgusted",
						value: fullFaceDescriptions[0].expressions.disgusted
					})
					arr.push({
						key: "fearful",
						value: fullFaceDescriptions[0].expressions.fearful
					})
					arr.push({
						key: "happy",
						value: fullFaceDescriptions[0].expressions.happy
					})
					arr.push({
						key: "neutral",
						value: fullFaceDescriptions[0].expressions.neutral
					})
					arr.push({
						key: "sad",
						value: fullFaceDescriptions[0].expressions.sad
					})
					arr.push({
						key: "surprised",
						value: fullFaceDescriptions[0].expressions.surprised
					})

					var max = arr[0].value;
					for(i=1;i<arr.length; i++)
					{
						if (arr[i].value > max)
							{
								max = arr[i].value;
								expressions = arr[i].key;
							}
					}

					const results = fullFaceDescriptions.map(fd => faceMatcher.findBestMatch(fd.descriptor));

					results.forEach((result, i) => {
						console.log(result._label);
						console.log(result._distance);	
    				});
					
					if(results.length == 1){
						var msg = { status : "ok", studId : results[0]._label, distance : results[0]._distance};						               
						console.log(msg);	
					}
					else
					{
						if(results.length > 0)
						{
							var msg = {
								status : "more",
								info : "More faces. Required 1 face"
							};
							console.log("err: more fasces, required 1 face");					 
						}
						else
						{														
							var msg = { status : "ok",studId : "unknow", distance : 1 };
							console.log(msg);
						}						                  	
					}

					response.end(JSON.stringify(msg));
					fs.unlinkSync(ImageFilePath);
					console.log(`delete file ${ImageFilePath}`);     				    
				}				
			});
			db.close();
		}

		await mongoClient.connect(urlDataBase, function(err, db){
			if(err){
				throw err;
			}else{
				console.log("connect to database done" + urlDataBase);
				db.collection(collectionDataBase).findOneAndUpdate(
					{studId: request.body.studId},
					{$set: {
						age: age,
						gender: gender,
						expressions: JSON.stringify(expressions),
						faceinrect: JSON.stringify(faceinrect)
					}},
					{returnOriginal: false, upsert: true},
					function(err, result){
						console.log("ok:"+result.ok+" "+JSON.stringify(result.lastErrorObject));
					}
				);
				db.close();
			}
		});
		
	});
});

app.post("/recognitionfast", jsonParser, async function(request, response){
	//параметры request.body
	//	password - пароль
	//	studId - имя/id-студента
	//	type - расширение изображения (без точки)
	//	image - изображение (строка base64)
	//console.log(request.body);
	const displaySize = {height: 200, width: 250, x: 20, y: 20};
	var box = new faceapi.Box(displaySize);
	var faceinrect = "";
	response.setHeader('Content-Type','application/json');//?
	if(!request.body) return response.sendStatus(400);
	var md5Hash = crypto.createHash('md5')
			.update(request.body.password)
			.update(salt)
			.digest('base64');
	if(md5Hash != password){
		console.log('wrong password');
		var rezult = {
			status: "err",
			info: "wrong password"
		};
		response.end(JSON.stringify(rezult));
		return;
	}
	try{
		//await faceapi.nets.tinyFaceDetector.loadFromDisk('../face/commons/weights');
		//await faceapi.nets.ssdMobilenetv1.loadFromDisk('../face/commons/weights');
		await faceapi.nets.mtcnn.loadFromDisk('../face/commons/weights');
		await faceapi.nets.faceRecognitionNet.loadFromDisk('../face/commons/weights');
		await faceapi.nets.faceLandmark68Net.loadFromDisk('../face/commons/weights');
	}
	catch(e){
		console.log('Ошибка в загрузке моделей!');
	}
	console.log("connect to database...");
	mongoClient.connect(urlDataBase, async function(err, db){
		if(err){
			throw err;
			console.log("err");
		}
		else{
			console.log("connect to database done");;
			db.collection(collectionDataBase)
                          .find({studId: request.body.studId},{frModel: 1}) 
                          .toArray(async function(err, stud){
				if(stud.length == 0){
				   var msg = {
					status : "unknow",
					studId : "unknow",
					distance : 1,
					info: "don`t find student "+request.body.studId +" on database"
				   };
				   console.log("don`t find student "+request.body.studId +" on database");
				   response.end(JSON.stringify(msg));
				}
				else{
					console.log("find stud "+request.body.studId);
					
					let model = JSON.parse(stud[0].frModel);			
					let descriptors = [];

					for(i=0;i<model.length;i++){
						descriptors[i] = await new Float32Array(Object.values(model[i]));
					}
					
					const modelFaceDescriptors = await new faceapi.LabeledFaceDescriptors(request.body.studId, descriptors);
					const faceMatcher = new faceapi.FaceMatcher(modelFaceDescriptors, 0.6)

                    var ImageFilePath = './images/'+request.body.studId +'.'+ request.body.type;
                    var buff = Buffer.from(request.body.image.replace(/^data:image\/(png|gif|jpeg);base64,/,''),'base64');
                    fs.writeFileSync(ImageFilePath, buff);
					console.log(`file ${ImageFilePath} upload`);

					console.log("recognition begin...");

					var image = await commons.canvas.loadImage(ImageFilePath);

					console.log('detecting faces for image...');
				
					var fullFaceDescriptions = await faceapi.detectAllFaces(image, new faceapi.MtcnnOptions())
															.withFaceLandmarks()
															.withFaceDescriptors();

					if (box.x <= fullFaceDescriptions[0].detection._box.x &&
						box.y <= fullFaceDescriptions[0].detection._box.y &&
						(fullFaceDescriptions[0].detection._box.x + fullFaceDescriptions[0].detection._box.width) <= (box.x + box.width) &&
						(fullFaceDescriptions[0].detection._box.y + fullFaceDescriptions[0].detection._box.height) <= (box.y + box._height))
								faceinrect = "yes";
					else
						faceinrect = "no";			

					const results = fullFaceDescriptions.map(fd => faceMatcher.findBestMatch(fd.descriptor));

					results.forEach((result, i) => {
						console.log(result.label);
						console.log(result.distance);
    				});
					
					if(results.length == 1){
						var msg = { status : "ok", studId : results[0]._label, distance : results[0]._distance};						               
						console.log(msg);	
					}
					else
					{
						if(rezults.length > 1)
						{
							var msg = {
								status : "more",
								info : "More faces. Required 1 face"
							};
							console.log("err: more fasces, required 1 face");
							DeleteStudent(stud);		 
						}
						else
						{							
							var msg = { status : "ok",studId : "unknow", distance : 1 };
							console.log(msg);
						}						                  	
					}   

					response.end(JSON.stringify(msg));
					fs.unlinkSync(ImageFilePath);
					console.log(`delete file ${ImageFilePath}`);      
				}				
			});
			db.close();
		}
		
		await mongoClient.connect(urlDataBase, function(err, db){
			if(err){
				throw err;
			}else{
				console.log("connect to database done" + urlDataBase);
				db.collection(collectionDataBase).findOneAndUpdate(
					{studId: request.body.studId},
					{$set: {								
						faceinrect: JSON.stringify(faceinrect)
					}},
					{returnOriginal: false, upsert: true},
					function(err, result){
						console.log("ok:"+result.ok+" "+JSON.stringify(result.lastErrorObject));
					}
				);
				db.close();
			}
		});
	});
});

app.post("/deleteStudent", jsonParser, async function(request, response) {
    //удаление студента (ответ серверу если такого студента не существует?)
	//	password - пароль
	//	studId - имя/id-студента
    var md5Hash = crypto.createHash('md5')
			.update(request.body.password)
			.update(salt)
			.digest('base64');
    if(md5Hash != password){
		console.log('wrong password');
		var rezult = {
			status: "err",
			info: "wrong password"
		};
		response.end(JSON.stringify(rezult));
		return;
    }
    mongoClient.connect(urlDataBase, function(err, db){
        if(err){
            throw err;
        }else{
            console.log("connect to database done");
            console.log(urlDataBase);
            db.collection(collectionDataBase)
              .find({studId: request.body.studId},{studId: 1})
              .toArray(function(err, stud){
                  if(stud.length == 0){
			var msg = {
				status : "ok",
				info: "don`t find student on database"
			};
			console.log("don`t find student on database");
			response.end(JSON.stringify(msg));
			db.close();
			return;
		  }
            });
            db.collection(collectionDataBase).findOneAndDelete({studId: request.body.studId},function(err,result){
                console.log(request.body.studId + ' deleted');
                var msg = {
                    status: 'ok',
                    info: 'stud '+request.body.studId + ' deleted'
                }
                response.end(JSON.stringify(msg));
                db.close();
            });
        }
    });
});

app.listen(port, () => console.log('Listening on port ' + port + '!'));
